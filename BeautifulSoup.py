#!/usr/bin/env python
# coding: utf-8

# # Walt Disney Pictures films Dataset Creation 
# ##### Scrape & Clean "List of Walt Disney Pictures films" Wikipedia Page for further analysis

# ## Task One: Get "Info Box" (Store in Python Dictionary)

# #### Import Required libraries

# In[137]:


from bs4 import BeautifulSoup as bs
import requests


# #### Load the Wikipedia Webpage

# In[138]:


r = requests.get("https://en.wikipedia.org/wiki/Toy_Story_3")


# ##### Convert to a Beautiful Soup Object

# In[139]:


soup = bs(r.content)


# #### Print the HTML

# In[140]:


contents = soup.prettify()
print(contents)


# In[141]:


info_box = soup.find(class_="infobox vevent")
#print(info_box.prettify())
info_rows = info_box.find_all('tr')
for row in info_rows:
    print(row.prettify())


# In[142]:


def get_content_value(row_data):
    if row_data.find('li'):
        return[li.get_text(" ", strip = True).replace("\xa0", " ") for li in row_data.find_all('li')]
    else:
        return row_data.get_text(" ", strip = True).replace("\xa0", " ")


movie_info = {}
for index, row in enumerate(info_rows):
    if index == 0:
        movie_info['title'] = row.find('th').get_text(" ", strip = True)
    elif index == 1:
            continue
    else:
        content_key = row.find('th').get_text(" ", strip = True)
        content_value = get_content_value(row.find('td'))
        movie_info[content_key] = content_value

movie_info


# ## Task Two: Get "Info Box" for all Movies

# In[143]:


r = requests.get("https://en.wikipedia.org/wiki/List_of_Walt_Disney_Pictures_films")
soup = bs(r.content)
contents = soup.prettify()
print(contents)


# In[144]:


movies = soup.select('.wikitable.sortable i')
movies[0:10]
#movies[0]
#movies[0].a['title']


# In[145]:


def get_content_value(row_data):
    if row_data.find('li'):
        return[li.get_text(" ", strip = True).replace("\xa0", " ") for li in row_data.find_all('li')]
    elif row_data.find('br'):
        return[text for text in row_data.stripped_strings]
    else:
        return row_data.get_text(" ", strip = True).replace("\xa0", " ")

def clean_tags(soup):
    for tag in soup.find_all(["sup", "span"]):
        tag.decompose()

def get_info_box(url):

    r = requests.get(url)
    soup = bs(r.content)
    info_box = soup.find(class_="infobox vevent")
    info_rows = info_box.find_all('tr')

    clean_tags(soup)

    movie_info = {}
    for index, row in enumerate(info_rows):
        if index == 0:
            movie_info['title'] = row.find('th').get_text(" ", strip = True)
        else:
            header = row.find('th')
            if header:
                content_key = row.find('th').get_text(" ", strip = True)
                content_value = get_content_value(row.find('td'))
                movie_info[content_key] = content_value

    return movie_info


# In[146]:


get_info_box('https://en.wikipedia.org/wiki/One_Little_Indian_(film)')


# In[147]:


r = requests.get("https://en.wikipedia.org/wiki/List_of_Walt_Disney_Pictures_films")
soup = bs(r.content)
movies = soup.select('.wikitable.sortable i a')
#print(len(movies))
base_path = "https://en.wikipedia.org/"

movie_info_list = []
for index, movie in enumerate(movies):
   # if index == 10:
    #    break
    try:
        relative_path = movie['href']
        full_path = base_path + relative_path
        title = movie['title']

        movie_info_list.append(get_info_box(full_path))

    except Exception as e:
        print(movie.get_text())
        print(e)


# ### Save / Reload Data

# In[153]:


import json

def save_data(title, data):
    with open(title, 'w', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=2)


# In[154]:


import json

def load_data(title):
    with open(title, encoding='utf-8') as f:
        return json.load(f)


# In[155]:


save_data('Disney_data_cleaned.json', movie_info_list)


# In[158]:


import pandas as pd

df = pd.DataFrame(movie_info_list)


# In[159]:


df.head()


# In[160]:


df.to_csv("disney_movies_final.csv")

